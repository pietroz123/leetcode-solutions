/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    
    public void insert(ListNode l, int value) {
        
    }
    
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        
        ListNode dummy = new ListNode(-1);
        ListNode head = dummy;
        int carry = 0;
        
        while (l1 != null || l2 != null) {
            
            int a = l1 != null ? l1.val : 0;
            int b = l2 != null ? l2.val : 0;
            int sum = a + b + carry;
            
            carry = sum >= 10 ? 1 : 0;
            
            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
            
            ListNode newNode = new ListNode(sum % 10);
            dummy.next = newNode;
            dummy = dummy.next;
        }
        
        if (carry == 1) {
            ListNode newNode = new ListNode(1);
            dummy.next = newNode;
        }
        
        return head.next;        
        
    }
    
}