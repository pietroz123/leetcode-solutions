class Solution {
    public int coinChange(int[] coins, int amount) {
        
        // ma for Minimum Amounts
        // Dynamic Programming: divide the problem into smaller subproblems which we can 
        // solve easily
        int[] ma = new int[amount + 1];
        
        // Fill the array with invalid amount, so we can know it's invalid
        Arrays.fill(ma, amount + 1);
        
        // The minimum amount to make change for 0 will always be 0
        ma[0] = 0;
        
        // i represents the amount from 1 to the desired amount
        for (int i = 1; i < ma.length; i++) {
            
            // Loop through each coin
            for (int j = 0; j < coins.length; j++) {
                
                // Check if the current coin can be used to make change
                // (if the current coin does not exceed the current amount)
                if (coins[j] <= i) {
                    // The +1 means we are using one coin
                    ma[i] = Math.min(ma[i - coins[j]] + 1, ma[i]);  
                }
                
            }
            
        }
        
        System.out.println(Arrays.toString(ma));
        
        // Return the amount of coins to make change for the desired amount
        return ma[amount] > amount ? -1 : ma[amount];
        
    }
}

/*

[1, 2, 5]

DP -> minimum amount for each amount from 0 to amount

min amount of coins -> [0 1 1 2 2 1 2 2 3 3  2  3]
     current amount ->  0 1 2 3 4 5 6 7 8 9 10 11

*/