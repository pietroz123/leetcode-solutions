class Solution {
    public int minMeetingRooms(int[][] intervals) {
        
        if (intervals == null || intervals.length == 0)
            return 0;
        
        // Sort intervals by start time
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        
        // Create a priority queue (heap) by end time
        PriorityQueue<Integer> q = new PriorityQueue<>((a, b) -> a - b);
        
        for (int[] interval: intervals) {
            
            // Check if room can acommodate meeting
            if (!q.isEmpty() && interval[0] >= q.peek()) {
                q.remove();
            }
            
            q.add(interval[1]);
        }
        
        return q.size();
        
    }
}