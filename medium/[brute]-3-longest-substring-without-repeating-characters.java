class Solution {
    public int lengthOfLongestSubstring(String s) {
        
        int max = Integer.MIN_VALUE;
        
        for (int i = 0; i < s.length(); i++) {
            
            String substring = "";
            
            for (int j = i; j < s.length(); j++) {
                
                substring += s.charAt(j);
                
                if (hasRepeatingChars(substring)) break;
                
                if (substring.length() > max) {
                    max = substring.length();
                }
                
            }
            
        }
        
        return max;
        
    }
    
    public boolean hasRepeatingChars(String s) {

        HashMap<Character, Integer> map = new HashMap<>();
        
        for (int i = 0; i < s.length(); i++) {
            
            char cur = s.charAt(i);
            
            if (map.get(cur) != null) {
                return true;
            }
            
            map.put(cur, 1);
            
        }
        
        return false;
        
    }
}

/*

1) Brute force

find each substring and check if it has repeating characters
if not, check if its length is more than max and make it max
return max

a           1
ab          2
abc         3
abca        repeats
abcab       repeats
abcabc      repeats
abcabcb     repeats
abcabcbb    repeats
b           1
bc          2
bca         3
bcab        repeats
...

Optimization
break as soon as a 'repeats' appears

loop through each combination:      O(n^2)
check if has repeating characters:  O(n)

-> O(n^3)

*/