class Solution {
    public int numIslands(char[][] grid) {
        
        if (grid == null || grid.length == 0) {
            return 0;            
        }
        
        int numIslands = 0;
        
        // Traverse the grid
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                
                // We only care about 'land'
                if (grid[i][j] == '1') {
                    numIslands += dfs(grid, i, j);
                }
                
            }
        }
        
        return numIslands;
    }
    
    private int dfs(char[][] grid, int i, int j) {
        
        // Stop if out of bounds or found water
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length || grid[i][j] == '0') {
            return 0;
        }
        
        // Mark position as 'visited'
        grid[i][j] = '0';
        
        // Traverse to neighbours
        dfs(grid, i, j + 1);        // right
        dfs(grid, i, j - 1);        // left
        dfs(grid, i + 1, j);        // up
        dfs(grid, i - 1, j);        // down
        
        return 1;
        
    }
}