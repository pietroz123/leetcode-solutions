class Solution {
    public int lengthOfLongestSubstring(String s) {
        
        HashSet<Character> set = new HashSet<>();
        int lp = 0, rp = 0;
        int max = 0;
        
        while (rp < s.length()) {
            
            if (!set.contains(s.charAt(rp))) {
                set.add(s.charAt(rp));
                rp++;
                max = Math.max(set.size(), max);
            }
            else {
                set.remove(s.charAt(lp));
                lp++;
            }
            
        }
        
        return max;
        
    }
}

/*

1) Brute force

find each substring and check if it has repeating characters
if not, check if its length is more than max and make it max
return max

a           1
ab          2
abc         3
abca        repeats
abcab       repeats
abcabc      repeats
abcabcb     repeats
abcabcbb    repeats
b           1
bc          2
bca         3
bcab        repeats
...

Optimization
break as soon as a 'repeats' appears

loop through each combination:      O(n^2)
check if has repeating characters:  O(n)

-> O(n^3)

2) Sliding Window
    - two pointers at the beggining
    - keep expanding right pointer until repeating char in set is found
    - if rep char -> remove first from set and update left pointer

time: O(n)

*/