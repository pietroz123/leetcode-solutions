class LRUCache {

    private HashMap<Integer, Item> map;
    private LinkedList<Item> list;
    private int capacity;
    private int size;

    public LRUCache(int capacity) {
        this.map = new HashMap<>();
        this.list = new LinkedList<>();
        this.capacity = capacity;
        this.size = 0;
    }

    public int get(int key) {

        // If key exists in map, return its value and put it on front of list
        if (map.get(key) != null) {

            Item item = map.get(key);
            list.remove(item);
            list.add(item);
            return item.value;

        }

        // Else, return -1 (not found)
        return -1;

    }

    public void put(int key, int value) {

        // If item is found in map, update its value
        if (map.get(key) != null) {

            Item item = map.get(key);
            list.remove(item);

            Item newItem = new Item(key, value);
            list.add(newItem);
            map.replace(key, newItem);

        } else {

            Item newItem = new Item(key, value);

            // If there is capacity, add to front of list
            if (size < capacity) {
                map.put(key, newItem);
                list.add(newItem);
                size++;
            }
            // Else, remove last from list (least recently used) and add the new item to the front
            else {
                Item removed = list.pollFirst();
                list.add(newItem);
                map.remove(removed.key);
                map.put(key, newItem);
            }

        }
    }

}

class Item {
    int key;
    int value;

    public Item(int key, int value) {
        this.key = key;
        this.value = value;
    }

}