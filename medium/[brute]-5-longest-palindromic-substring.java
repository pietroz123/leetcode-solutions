class Solution {

    public String longestPalindrome(String s) {
        
        String longestSubstring = "";
        int maxLength = Integer.MIN_VALUE;

        for (int i = 0; i < s.length(); i++) {

            String subString = "";
            
            for (int j = i; j < s.length(); j++) {
                
                subString += s.charAt(j);
                System.out.println(subString);
                if (isPalindrome(subString)) {
                    if (subString.length() > maxLength) {
                        maxLength = subString.length();
                        longestSubstring = subString;
                    }
                }

            }
        }

        return longestSubstring;
    }

    public Boolean isPalindrome(String str) {

        int start = 0, end = str.length() - 1;

        while (start < end) {
            if (str.charAt(start) != str.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }

        return true;

    }
    
}

class Main {
    public static void main(String[] args) {

        Solution s = new Solution();

        System.out.println(s.longestPalindrome("babad"));

    }

}