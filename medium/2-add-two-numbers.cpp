/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    void insert(ListNode* l, int value) {
        if (l->val == -1) {
            l->val = value;
            return;
        }
        
        while (l->next != NULL) {
            l = l->next;
        }
        
        ListNode* newNode = new ListNode(value);
        l->next = newNode;        
    }
    
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        
        ListNode* result = new ListNode(-1);
        int carry = 0;
        
        while (l1 != NULL || l2 != NULL) {
            
            int a = (l1) ? l1->val : 0;
            int b = (l2) ? l2->val : 0;
            
            int sum = a + b + carry;
            carry = (sum >= 10) ? 1 : 0;
            
            insert(result, sum % 10);
            
            if (l1) l1 = l1->next;
            if (l2) l2 = l2->next;            
            
        }
        
        if (carry) insert(result, 1);
        
        return result;
        
    }
};