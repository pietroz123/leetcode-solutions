class Solution {
    public int connectSticks(int[] sticks) {
        
        int minCost = 0;
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        
        for (int stick: sticks) heap.add(stick);
        
        while (heap.size() > 1) {
            int sum = heap.remove() + heap.remove();
            minCost += sum;
            heap.add(sum);
        }
        
        return minCost;
        
    }
}