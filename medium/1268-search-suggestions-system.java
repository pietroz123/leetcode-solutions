class Solution {
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        
        List<List<String>> suggestedProducts = new ArrayList<List<String>>();
        
        String searchString = "";
        for (int i = 0; i < searchWord.length(); i++) {
            
            searchString += searchWord.charAt(i);
            List<String> searchList = new ArrayList<>();
            
            for (int j = 0; j < products.length; j++) {
                
                if (checkPrefix(products[j], searchString)) {
                    searchList.add(products[j]);
                }
                
            }
            
            Collections.sort(searchList);
            
            int x = searchList.size() < 3 ? searchList.size() : 3;
            searchList = searchList.subList(0, x);
            
            suggestedProducts.add(searchList);
        }
        
        return suggestedProducts;
        
    }
    
    private Boolean checkPrefix(String str, String check) {
        int i = 0;
        int j = 0;
        int count = 0;
        
        while (i < str.length() && j < check.length()) {
            if (!(str.charAt(i) == check.charAt(j))) {
                break;
            }
            i++;
            j++;
            count++;
        }
        
        if (count == check.length()) {
            return true;
        }
        
        return false;
    }
    
}