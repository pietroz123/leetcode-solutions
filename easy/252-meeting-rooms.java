class Solution {
    public boolean canAttendMeetings(int[][] intervals) {
        
        if (intervals == null || intervals.length == 0)
            return true;
        
        int[] starts = new int[intervals.length];
        int[] ends = new int[intervals.length];
        
        for (int i = 0; i < intervals.length; i++) {
            starts[i] = intervals[i][0];
            ends[i] = intervals[i][1];
        }
        
        Arrays.sort(starts);
        Arrays.sort(ends);
        
        for (int i = 0; i < starts.length - 1; i++) {
            if (ends[i] > starts[i + 1]) {
                return false;
            }
        }
        
        return true;
    }
}