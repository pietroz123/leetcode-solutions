class Solution {
    public boolean canAttendMeetings(int[][] intervals) {
        
        if (intervals == null || intervals.length == 0)
            return true;
        
        int maxEnd = Integer.MIN_VALUE;
        for (int i = 0; i < intervals.length; i++) {
            
            int end = intervals[i][1];

            if (end > maxEnd) {
                maxEnd = end;
            }

        }
        
        int[] check = new int[maxEnd];
        
        for (int i = 0; i < intervals.length; i++) {
            
            int start = intervals[i][0];
            int end = intervals[i][1];

            for (int j = start; j <= end-1; j++) {
                if (check[j] == 1) {
                    return false;
                }
                check[j] = 1;
            }
            
        }
        
        return true;
    }
}