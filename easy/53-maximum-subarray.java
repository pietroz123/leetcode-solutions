class Solution {
    public int maxSubArray(int[] nums) {
        
        int len = nums.length;
        int[] aux = new int[len];
        
        // Insert first element
        aux[0] = nums[0];
        
        // Solve with dynamic programming
        for (int i = 1; i < len; i++) {
            aux[i] = Math.max(nums[i], nums[i] + aux[i-1]);
        }

        // Get max value
        int max = Integer.MIN_VALUE;
        for (int num: aux) {
            if (num > max) {
                max = num;
            }
        }
        
        return max;
        
    }
}