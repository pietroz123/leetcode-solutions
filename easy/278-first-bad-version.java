/* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

public class Solution extends VersionControl {
    
    private int bSearch(int start, int end) {
        
        if (start == end) {
            return start;
        }
        
        int mid = start + ((end - start) / 2);
        
        if (isBadVersion(mid)) {
            return bSearch(start, mid);
        }
        else {
            return bSearch(mid + 1, end);
        }
        
    }
    
    public int firstBadVersion(int n) {
        return bSearch(1, n);
    }
}

/*

O(logn)

*/