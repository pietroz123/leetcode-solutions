class Solution {
    public int singleNumber(int[] nums) {
        
        HashMap<Integer, Integer> map = new HashMap<>();
        
        for (int i = 0; i < nums.length; i++) {
            int current = nums[i];
            if (!map.containsKey(current)) {
                map.put(current, 1);
            }
            else {
                int qty = map.get(current);
                map.put(current, qty + 1);
            }
        }
        
        for (int num: map.keySet()) {
            int qty = map.get(num);
            if (qty == 1) {
                return num;
            }
        }
        
        return -1;
    }
}