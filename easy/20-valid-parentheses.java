class Solution {
    public boolean isValid(String s) {

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            
            char current = s.charAt(i);

            if (current == '(' || current == '[' || current == '{') {
                stack.push(current);
            }
            else if (current == ')' && !stack.empty() && stack.peek() == '(') {
                stack.pop();
            }
            else if (current == ']' && !stack.empty() && stack.peek() == '[') {
                stack.pop();
            }
            else if (current == '}' && !stack.empty() && stack.peek() == '{') {
                stack.pop();
            }
            else {
                return false;
            }

        }

        if (stack.empty()) {
            return true;
        }

        return false;
    }
}