class Solution {
    public int firstUniqChar(String s) {
        
        HashMap<Character, Integer> map = new HashMap<>();
        
        // Iterate through the string
        for (int i = 0; i < s.length(); i++) {
            char current = s.charAt(i);
            if (!map.containsKey(current)) {
                map.put(current, i);
            }
            else {
                map.put(current, -1);
            }
        }
        
        // Return the letter with the lowest index
        int min = Integer.MAX_VALUE;
        for (char c: map.keySet()) {
            int index = map.get(c);
            if (index != -1 && index < min) {
                min = index;
            }
        }
        
        // Return
        return min == Integer.MAX_VALUE ? -1 : min;
    }
}