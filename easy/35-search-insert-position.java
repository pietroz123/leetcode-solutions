class Solution {
    public int searchInsert(int[] nums, int target) {

        return bSearch(nums, target, 0, nums.length-1);
        
    }
    
    private int bSearch(int[] nums, int key, int start, int end) {
        
        // Reached end of search and still not found
        if (start == end) {
            if (key <= nums[start])
                return start;
            else
                return start+1;
        }
        
        int mid = (start + end) / 2;
        
        // Found key
        if (key == nums[mid]) {
            return mid;
        }
        // Didn't find key
        else {
            if (key < nums[mid]) {
                // Call to Left
                return bSearch(nums, key, start, mid);
            }
            else {
                // Call to right
                return bSearch(nums, key, mid + 1, end);
            }
        }
        
    }
}

/*

time: O(logn)
space: O(1)

*/