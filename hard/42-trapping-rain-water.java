class Solution {
    public int trap(int[] height) {
        
        if (height == null || height.length == 0)
            return 0;
        
        int totalWater = 0;
        
        
        for (int i = 0; i < height.length; i++) {
            int currentHeight = height[i];
            int amountItCanTrap = Math.min(leftHighest(height, i), rightHighest(height, i)) - currentHeight;
            totalWater += amountItCanTrap;
        }
        
        return totalWater;
    }
    
    private int leftHighest(int[] map, int index) {
        int highest = Integer.MIN_VALUE;
        for (int i = index; i >= 0; i--) {
            if (map[i] > highest) {
                highest = map[i];
            }
        }
        return highest;
    }
    
    private int rightHighest(int[] map, int index) {
        int highest = Integer.MIN_VALUE;
        for (int i = index; i < map.length; i++) {
            if (map[i] > highest) {
                highest = map[i];
            }
        }
        return highest;
    }
}